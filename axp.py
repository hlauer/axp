#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (C) 2020-2023 Hermann Lauer
# can be distributed under the GPL V3 or later

"""NAME
        %(prog)s - read AXP adc values
    
SYNOPSIS
        %(prog)s [--help]
        
DESCRIPTION
        tbd
FILES  
        tbd          
     
SEE ALSO
        nothing
        
DIAGNOSTICS
        none
        
BUGS    
        many
    
AUTHOR
        Hermann.Lauer@uni-heidelberg.de
        {version}
"""

version="$Id: vitro.py fc0720140421 2018/02/18 18:09:16 Hermann $"
import syslog

class Value:
    """calulate a value"""

    def __init__(s,path):
        """store offset and scale"""

        s.path=path+'_'
        s.scale=float(open(s.path+'scale').read())
        s.offset=0
        try:
          s.offset=int(open(s.path+'offset').read())
        except IOError: pass

    def __call__(s):
        raw=int(open(s.path+'raw').read())
        return (raw+s.offset)*s.scale

class AXP(dict):
    names={'acinV':'voltage0',
	'acinI':'current0',
	'vbusV':'voltage1',
	'vbusI':'current1',
	'temp':'temp',
	'gpio0V':'voltage3',
	'gpio1V':'voltage4',
	'ipsoutV':'voltage5',
	'batV':'voltage6',
	'batOI':'current2',
	'batII':'current3',
	}

    def __init__(s,path='/sys/bus/platform/devices/axp20x-adc/iio:device0/in_'):
        s.mess=[(k,Value(path+v)) for k,v in s.names.items()]

    def read(s):
        for k,v in s.mess:
          cnt=1
          while True:
           try:
             s[k]=v()
             break
           except TimeoutError as e:
             print("read{}({}) {}".format(cnt,k,e))
             cnt+=1
        s['batI']=s['batII']-s['batOI']
        return s

    def buildpkt(s):
        return "acin {0[acinV]:.1f} {0[acinI]}\nvbus {0[vbusV]:.1f} {0[vbusI]}\nTipsV {0[temp]:.0f} {0[ipsoutV]:.1f}\nbat {0[batV]:.1f} {0[batI]:.1f}".format(s)

class AXPmmap(AXP):
    def __init__(s,path='/sys/bus/platform/devices/axp20x-adc/iio:device0/in_'):
      super().__init__(path)
      try:
        from axpmmap import axpow
      except (ImportError,FileNotFoundError) as e:
        print(e)
        class c: pass                   #define a dummy
        axpow=c
      s.shm=axpow
      s.shmkeys=[k[0] for k in s.shm._fields_ if k[0] not in ("nextsched","batI")]

      def read(s):
        for k,v in s.mess:
          s[k]=v()
          setattr(s.shm,k,s[k])         #unused keys stay locally
        s.shm.batI=s['batII']-s['batOI']
        return s.buildpkt()

class axpUDP():
  def __init__(s,host='10.0.19.6',port=4330):
    import socket, syslog
    try:
      s.port = port
      _,_,ips = socket.gethostbyaddr(host)
      s.host = ips[0]
    except Exception as e:
#      syslog.syslog(syslog.LOG_ERR,"Could not resolve host: {}".format(host))
      sys.stderr.write("Could not resolve host({}): {}".format(e,host))
      s.host='10.0.19.6'

  def send(s,data):
    import socket, syslog
    so = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    so.connect((s.host, s.port))
    so.send(data)
    so.close()

import threading
class Logging(threading.Thread):
  """loop to log"""

  def __init__(s,axp):
    import select
    super(Logging, s).__init__()
    s.daemon=True 
    s.axp=axp
    s.axpsoc=None
    s.power=None
    s.period=1
    s.poll=select.poll()
    s.debug=args.debug

  def waitexec(s,rettime):
    import time,select,syslog

    while True:
      delay=rettime-time.monotonic()
      if delay<0: return
      if s.debug:
          print("{} axp loop: {}".format(time.asctime(),delay))
      ret=s.poll.poll(delay*1000)        #sleep
      if not ret: return
 
  def run(s):
    import time,sys
    starttime=0
    lastnetfail=None
    oldstate=None

    while True:
      cyclet=time.monotonic()

      s.axp.read()
      if s.debug>1:
          print(r)
      if s.power:
        try:
          state=s.power(s.axp)
          if state!=oldstate:
              print(s.axp)
              print(state, flush=True)   #print state change
              oldstate=state
          else:
              if s.debug: print(state)
          if state==(-1,'shutting down'): sys.exit(10)
        except OSError as e:    #e.errno 2 for not root setup
          sys.stderr.write("powercheck failed({}): {}".format(e,r))
      if s.axpsoc:
        r=s.axp.buildpkt()
        if 'do' in s.axp:
          r="do {}\n{}".format(s.axp['do'],r)
          del s.axp['do']
        try:
          s.axpsoc.send(bytes("{}".format(r),'ascii'))
          if lastnetfail:
            sys.stderr.write(lastnetfail)
            lastnetfail=None
        except OSError as e:
          if lastnetfail is None:
            lastnetfail="Send failed({}): {}".format(e,r)
            sys.stderr.write(lastnetfail)
          else:
            lastnetfail="Send last failed({}): {}".format(e,r)
      else:
          acin(s.axp)
          vbus(s.axp)
          bat({'batV':s.axp['batV'],'batI':s.axp['batII']-s.axp['batOI']})
          ipsoutV.store(s.axp['ipsoutV'])
          temp.store(s.axp['temp'])

      starttime+=s.period
      if starttime<cyclet: 	                #we missed to much, jump to real time
#        syslog.syslog(syslog.LOG_WARNING,"axp loop to slow by {0}s, jumped.".format(cyclet-starttime))
        if s.debug:
          print("axp loop to slow by {0}s, jumped.".format(cyclet-starttime))
        starttime=(int(cyclet)/s.period+1)*s.period+1   #delay by one second
      s.axp.shm.nextsched=int(starttime)
      s.waitexec(starttime)

#-------------------------------------------------------------------------------
import argparse
class MyHelpFormatter(argparse.RawTextHelpFormatter):
    # It is necessary to use the new string formatting syntax, otherwise
    # the %(prog) expansion in the parent function is not going to work
    def _format_text(self, text):
        if '{version}' in text:
            text = text.format(version=version)
        return super()._format_text(text)

def ManOptionParser():
	return argparse.ArgumentParser(description=__doc__, 
                                       formatter_class=MyHelpFormatter)

#-------------------------------------------------------------------------------
#    Main 
#-------------------------------------------------------------------------------
if __name__=="__main__":
    import sys

    parser = ManOptionParser()
    parser.add_argument('-d', dest='debug', type=int, default=0,
                        metavar='DEBUG', help='DEBUG LEVEL')
    parser.add_argument('-s', '--store', action='store_true', dest='store',
                        help="store locally instead of sending over network")

    args = parser.parse_args()
    if sys.flags.interactive and not args.debug: args.debug=1

    axp=AXPmmap()
    axpsoc=axpUDP()
#    syslog.openlog("axp", syslog.LOG_PERROR, syslog.LOG_LOCAL3)

    l=Logging(axp)    #prepare Thread
    if args.store:
        from whisperclass import WSPval
        class wspVI:
          """keep V and I as separate WSP
        """
          def __init__(s,name):
            path="wsp/power/{}/".format(name)
            s.V=WSPval(path+'V.wsp')
            s.I=WSPval(path+'I.wsp')
            s.name=name

          def __call__(s,vh):
            s.V.store(vh[s.name+'V'])
            s.I.store(vh[s.name+'I'])
        acin=wspVI('acin')
        vbus=wspVI('vbus')
        bat=wspVI('bat')
        ipsoutV=WSPval('wsp/power/ipsoutV.wsp')
        temp=WSPval('wsp/power/T.wsp')
    else: l.axpsoc=axpsoc
    try:
        import power
        p=power.PowerManager()
        l.power=p
    except Exception as e:
        sys.stderr.write("powerregulator failed: {}".format(e))
    if sys.flags.interactive: l.start()         #start it
    else: l.run()
