#!/usr/bin/python3
# (C) Hermann Lauer 2020-2021
# can be distributed under the GPL V3 or later
"""NAME
        %(prog)s - <one line short description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

CREDITS
        tbd

AUTHOR
        Hermann.Lauer@uni-heidelberg.de
        {version}
"""

from whisperclass import WSPval
class wspVI:
  """keep V and I as separate WSP
"""
  def __init__(s,name,path="wsp/pmic"):
    path="{}/{}/".format(path,name)
    s.V=WSPval(path+'V.wsp')
    s.I=WSPval(path+'I.wsp')
    s.name=name

  def __call__(s,vh):
    s.V.store(vh[s.name+'V'])
    s.I.store(vh[s.name+'I'])

class wspPMIC:
  """keep wsp set for a pmic
"""
  def __init__(s,path="wsp/pmic"):
    s.acin=wspVI('acin',path)
    s.vbus=wspVI('vbus',path)
    s.bat=wspVI('bat',path)
    s.ipsoutV=WSPval('{}/ipsoutV.wsp'.format(path))
    s.temp=WSPval('{}/T.wsp'.format(path))
    s.cnt=0

  def __call__(s,vh):
    s.acin(vh)
    s.vbus(vh)
    s.bat(vh)
    s.ipsoutV.store(vh['ipsoutV'])
    s.temp.store(vh['temp'])
    s.cnt+=1
    return s.cnt

class AXP(dict):
  def __init__(s):
    try:
        from axpmmap import axpow
    except (ImportError,FileNotFoundError) as e:
        print(e)
        class c: pass   #define a dummy
        axpow=c
    s.shm=axpow
# s.shmkeys=[k[0] for k in s.shm._fields_ if k[0] not in ("nextsched","batI")]

  def storeVI(s,data):
    k,V,I=data.split()
    V=float(V)
    I=float(I)
    s[k+'V']=V
    setattr(s.shm,k+'V',V)
    s[k+'I']=I
    setattr(s.shm,k+'I',I)

  def __call__(s,data):
    for l in data.split('\n'):
      if l.startswith('TipsV '):
        T,ipsV=l.split()[1:]
        T=float(T)
        ipsV=float(ipsV)
        s['temp']=T
        s.shm.temp=T
        s['ipsoutV']=ipsV
        s.shm.ipsoutV=ipsV
        continue
      s.storeVI(l)
    return s

if __name__=="__main__":
  import sys, socket, syslog, os
  syslog.openlog(os.path.basename(sys.argv[0]), 0, syslog.LOG_DAEMON)

  axp=AXP()

  HOST = ''                 # Symbolic name meaning the local host
  PORT = 4330               # Arbitrary non-privileged port (cmd: 4333)
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  try:
    s.bind((HOST, PORT))
  except socket.error as errex:
    syslog.syslog(syslog.LOG_ERR,"bind failed: %s"%(errex[1]))
    sys.stderr.write("bind failed: %s\n"%(errex[1]))
    sys.exit(1)

  os.environ["PYTHONINSPECT"] = "1"       #allow interactive debug

  wpdict={'10.0.19.9':wspPMIC(),
          '10.0.19.5':wspPMIC('wsp/bam'),
          }

  import powerio
  check={'10.0.19.9':lambda x:None,
         '10.0.19.5':powerio.radioPower(),
        }

  pktcnt=1
  while True:
    try:
      data,iaddr = s.recvfrom(1504)
      st=data.decode()
      vh=axp(st)
      addr,port=iaddr
      res=check[addr](vh)
      if res: print(res)
      print(vh,addr)
      pktcnt=wpdict[addr](vh)
    except (IOError,KeyError) as e:
      print(e)
