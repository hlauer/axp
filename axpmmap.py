# (C) Hermann Lauer 2020
# can be distributed under the GPL V3 or later

#structure to share axp data
from ctypes import Structure, c_int, c_uint, c_float

class AXPow(Structure):
    """values of axp Power manager"""
    _fields_ = [("nextsched", c_uint),("acinV",c_float),("acinI",c_float),('vbusV',c_float),
                ("vbusI",c_float),("temp",c_float),('ipsoutV',c_float),('batV',c_float),
                ('batI',c_float)]

from memmap import StructMap
axpow=StructMap(AXPow,"axpow.mmap")
