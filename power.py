#!/usr/bin/python3
# (C) Hermann Lauer 2021-2022
# can be distributed under the GPL V3 or later

"""NAME
        %(prog)s - <one line short description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

AUTHOR
        Hermann.Lauer@uni-heidelberg.de
        {version}
"""

class PowerPin:
  """base class provinding permission setup
"""
  def __init__(s,gpio=259,init=True):
    if init: s.setup(gpio)
    else: s.path="/sys/class/gpio/gpio{}/".format(gpio)

  def setup(s,gpio):
    import os, shutil, time
    path="/sys/class/gpio/"
    try:
      with open(path+"export","w") as f:
        f.write("{}".format(gpio))
      s.path="/sys/class/gpio/gpio{}/".format(gpio)
      for c in s.which:
        print(s.path+c)
        os.chmod(s.path+c,0o664)			#set group write
        shutil.chown(s.path+c,group="dialout")
    except OSError as e:
      print("Pin{}: {}".format(gpio,e))
      if e.errno!=16: print("Pin{}: {}".format(gpio,e))	#16: already setup
      else:
        if e.errno not in (13,16): return
      s.path="/sys/class/gpio/gpio{}/".format(gpio)	#no permissions, try to continue

  def toggle(s):
    if s.state: s.off()
    else: s.on()
    s.state=not s.state

class PowerPinOC(PowerPin):
  """stear OpenCollector power via a gpio: output 0 is on
"""
  def __init__(s,gpio,init=True):
    s.which = 'direction','value'
    super().__init__(gpio,init)
    with open(s.path+"direction","r") as f:
      s.state=f.read().startswith("out")

  def on(s):
    with open(s.path+"direction","w") as f:
      f.write("out")
    with open(s.path+"value","w") as f:
      f.write("0")

  def off(s):
    """define as input, so driving low ends
"""
    with open(s.path+"direction","w") as f:
      f.write("in")

class PowerPinO(PowerPin):
  """stear power via a gpio
"""
  def __init__(s,gpio,init=True):
    s.which = 'value',
    super().__init__(gpio,init)
    with open(s.path+"value","r") as f:
      s.state=f.read().startswith("1")

    if init:
      with open(s.path+"direction","w") as f:
        f.write("out")

  def on(s):
    with open(s.path+"value","w") as f:
      f.write("1")

  def off(s):
    with open(s.path+"value","w") as f:
      f.write("0")

class Battery:
  """set charge current
"""
  def __init__(s):
    s.path="/sys/class/power_supply/axp20x-battery/"
#    s.setmax(600)
    s.setperm()

  def setperm(s):
    import shutil,os
    try:
      for c in ("status",):
        os.chmod(s.path+c,0o664)			#set group write
        shutil.chown(s.path+c,group="dialout")
    except OSError as e:
      if e.errno!=16: print("Battery: {}".format(e))	#16: already setup
      else:
        if e.errno not in (13,16): return

  def setmax(s,max):
    import sys
    try:
      with open(s.path+"constant_charge_current_max","w") as f:
        f.write(str(max*1000))
      with open(s.path+"constant_charge_current","w") as f:
        f.write(str(max*1000))
      s.chargeI=max
    except OSError as e:
      sys.stderr.write("setmax({}) {}, Limit is:".format(max,e))
      with open(s.path+"constant_charge_current_max","r") as f:
        s.chargeI=int(f.read())/1000
        sys.stderr.write(" {}mA\n".format(s.chargeI))

  def on(s):
    with open(s.path+"status","w") as f:
      f.write("Charging")

  def off(s):
    with open(s.path+"status","w") as f:
      f.write("Not charging")

  def __call__(s):
    with open(s.path+"voltage_now","r") as f:
     with open(s.path+"status","r") as g:
      return int(f.read()),g.read().rstrip()

class Power:
  """coroutines to watch power, act as appropriate to conserve
"""
  def __init__(s):
    s.debug=0
    s.regel=s.check()
    s.regel.send(None)		#initialize generator

  def __call__(s,mess):
    try: return s.regel.send(mess)
    except StopIteration: return 'StopIteration'

class acMinMax(Power):
  """coroutine noting min/max values
"""

  def check(s):
    """follow acinV measurement values and yield min/max V
    max battery I and min acin I is also yield
"""
    minV=10000
    maxV=0
    maxI,maxI2=1,1
    acImin,acImin2=5000,5000
    V=0
    D=1                 #increasing or decreasing
    lastV=0
    lastI=0
    while True:
      lastV=V
      mess=yield D,minV,maxV,maxI2,acImin2
      V,acI,I=mess['acinV'],mess['acinI'],mess['batI']
      maxI=max(maxI,I)
      maxI2=maxI
      acImin=min(acImin,acI)
      acImin2=acImin
      if D>0:               #V was increasing
        if V<lastV:             #stopped now
          maxV=lastV;D=-1   #note maximum
#          print("max({},{}): {}".format(maxV,maxI,mess))
          maxI=0
      else:
        if V>lastV:
          minV=lastV;D=1    #note minimum
#          print("min({},{}): {}".format(minV,acImin,mess))
          acImin=5000

class PowerChill(Power):
  """Power up or down chiller
"""
  def __init__(s,init=True):
    s.ssr2=PowerPinO(35,init)	#B3
    s.p2=PowerPinOC(40,init)		#B8
    s.acmin=5200
    super().__init__()

  def cycle(s):
    import time
    s.ssr2.off()
    s.p2.off()
    time.sleep(1)
    s.ssr2.on()
    time.sleep(1)
    s.p2.on()

  def check(s):
    """getting measurement values and react
"""
    import time
    mess={'batI':1,'acinV':0}
    s.last=time.monotonic()

    while mess.get('OFF')!=True:
      if mess['acinV']>s.acmin and mess['batI']<=1.0:
       try:
#        s.ssr2.on()
        minV=6000
        maxI=0
        for i in range(5):
          mess=yield 1,'ssr2 switched{} on {}V {}V {}A'.format(i,s.acmin,minV,maxI)
          minV=min(minV,mess['acinV'])
          maxI=max(maxI,mess['batI'])
        if mess['acinV']>=s.acmin and minV>=s.acmin and maxI<=1.0:
#          s.p2.on()
          mess=yield 1,'p2 switched on'
          while mess['acinV']>=s.acmin and mess['batI']<=1.0: # and mess['vbusV']>0:
            mess=yield 2,'p2 on'
          s.p2.off()
          yield 1,'p2 switched off'
          s.ssr2.off()
          mess=yield 0,'ssr2 switched off'
       except OSError as e:
        mess=yield str(e)
      else: mess=yield 0,'p2 off'

class PowerSR(Power):
  """Power comes at morning, stop at afternoon
"""
  def __init__(s,init=True):
    s.b=Battery()
    s.ssr=PowerPinO(45,init)		#PB13
    super().__init__()
    s.p=PowerChill(init)
    s.MinMax=acMinMax()

  def check(s):
    """go through the day, getting measurement values
"""
    import time,sundata
    s.last=time.monotonic()
    sunset=sundata.Transit(time.time()/86400).set()
    del sundata

    while s.last+3600>time.monotonic() and time.time()<sunset:
      mess=yield 0,'starting unstable on battery'
      D,minV,maxV,maxI,acImin=s.MinMax(mess)
      if minV<4500 or maxI>1.0:
        s.last=time.monotonic()
#        print(s.last,time.time(),sunset,minV,maxI)
    if time.time()>sunset: yield -1,'shutting down'
    print(D,minV,maxV,maxI,acImin)

    while mess['batI']>0.5 or mess['acinV']<5050:
      if s.debug: print(mess)
      mess=yield 0,'waiting for battery charging power: {}'.format(s.p(mess))

    while True:
      try:
        s.b.on()		#start charging
        s.last=time.monotonic()
        yield 'charging is switched on'
        break
      except OSError as e:
        if s.b()[1]=='Full': break
        yield str(e)

    while s.b()[1]!='Full':
      if s.debug: print(mess)
      if mess['acinV']>0:
        mess=yield 1,'charging: {}'.format(s.p(mess))
      else:
        yield -1,'shutting down'
    try:
        s.b.off()		#stop charging
    except OSError as e:
        mess=yield str(e)

    while True:
      res=s.p(mess)
      if mess['acinV']<4962 and res==(0,'p2 off'): break
      mess=yield 2,'full up: {}'.format(res)
      s.p.acmin=5100

    s.ssr.off()
    mess=yield 1,'switched off ssr'

    while mess['batI']<=0.5:
      mess=yield 1,'going down'

    while mess['acinV']>0 and mess['batV']>3700:
      mess=yield 0,'rundown on battery'

    yield -1,'shutting down'

class PowerExtern(Power):
  """we are powered from elsewhere, manage additional power
"""
  def __init__(s,init=True):
    s.b=Battery()
    s.p=PowerPinO(228,init)		#PH4 (TXD, so high after restart)
    super().__init__()

  def check(s):
    """go through the day, getting measurement values
"""
    import time
    mess={'batOI':1,'acinV':0}
    s.last=time.monotonic()
    s.fullup=None

    while mess['batOI']>0.5 or mess['acinV']<4900:
      if mess['acinV']: print(mess)
      if mess['batOI']<=0.5: s.last=time.monotonic()
      if mess['acinV']>4900 and s.last+60<time.monotonic(): break
      mess=yield 0,'starting on battery'

    try:
#      s.p.on()			#start supply fully
#      yield 'power is switched on'
      yield 'power would be switched on'
    except OSError as e:
      yield str(e)

    while True:
      try:
        s.b.on()		#start charging
        s.last=time.monotonic()
        yield 'charging is switched on'
        break
      except OSError as e:
        if s.b()[1]=='Full': break
        yield str(e)

    while mess['acinV']>4800:
      mess=yield 2,'full up'

    while mess['batI']<=0.5:
      mess=yield 1,'going down'

    while mess['acinV']>0 and mess['batV']>3700:
      mess=yield 0,'rundown on battery'

def PowerManager(coldstart=False):
  import platform
  HostManager={'bam':PowerSR,
              'bapro':PowerExtern,
  }
  return HostManager[platform.node()](coldstart)

#-------------------------------------------------------------------------------
import argparse
class MyHelpFormatter(argparse.RawTextHelpFormatter):
    # It is necessary to use the new string formatting syntax, otherwise
    # the %(prog) expansion in the parent function is not going to work
    def _format_text(self, text):
        if '{version}' in text:
            text = text.format(version=version)
        return super(MyHelpFormatter, self)._format_text(text)

def ManOptionParser():
    return argparse.ArgumentParser(description=__doc__,
                                       formatter_class=MyHelpFormatter)

if __name__=="__main__":
  import sys
  parser = ManOptionParser()
  parser.add_argument('-p', '--permissions', action='store_true', dest='perm',
                      help="configure permissions for unpriviledged access")
  parser.add_argument('-s', dest='switch',action='store',
                      help='switch action')
  args = parser.parse_args()

  p=PowerManager(args.perm)
  if args.switch:
    if args.switch=="toggle": p.p.toggle()
