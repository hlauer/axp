#!/usr/bin/python3
# (C) Hermann Lauer 2021
# can be distributed under the GPL V3 or later

"""NAME
        %(prog)s - <one line short description>

SYNOPSIS
        %(prog)s [--help]

DESCRIPTION
        none

FILES
        none

SEE ALSO
        nothing

DIAGNOSTICS
        none

BUGS
        none

AUTHOR
        Hermann.Lauer@uni-heidelberg.de
        {version}
"""

class radioPower:
  def __init__(s):
    from power import acMinMax
    s.lastOn=0
    s.MinMax=acMinMax()

  def __call__(s,mess):
    import time
    D,minV,maxV,maxI,acImin=s.MinMax(mess)
    if minV>5100 and maxI<=0.5:
      t=time.time()
      if t-s.lastOn>10*3600:
        try:
          with open("/sys/class/gpio/gpio231/value","w") as f:    #H7
            f.write("1")
          s.lastOn=t
          print("H7 switched on {}".format(time.asctime()))
        except OSError as e: print(e)
    elif maxV<4000 and maxI>10:
      if time.time()-s.lastOn<20*3600:          #switched on in less the 20h
        try:
#          with open("/sys/class/gpio/gpio231/value","w") as f:    #H7
#            f.write("0")
          print("H7 would switched off {}".format(time.asctime()))
        except OSError as e: print(e)
    else: print(D,minV,maxV,maxI,acImin)
